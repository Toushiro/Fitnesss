import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Form from '../views/Form.vue'
import Trainings from '../views/Trainings.vue'
import Training from '../views/Training.vue'
import Results from '../views/Results.vue'
import ProgramCreateForm from '../views/ProgramCreateForm.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/form',
    name: 'Form',
    component: Form,
  },
  {
    path: '/trainings',
    name: 'Trainings',
    component: Trainings,
  },
  {
    path: '/trainings/:id',
    name: 'Training',
    component: Training,

  },
  {
    path: '/results',
    name: 'Results',
    component: Results
  },
  {
    path: '/create',
    name: 'ProgramCreateForm',
    component: ProgramCreateForm
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
