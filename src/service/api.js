import axios from 'axios'

const baseURL = `https://605c4b066d85de00170d9c68.mockapi.io`
const api = axios.create({ baseURL })

function loadExercises(id) {
  console.log(id)
  return new Promise((resolve, reject) => {
    api
      .get(`/Exercises`)
      .then((r) => {
       

        if (r.status === 200) {
          resolve(r.data)
        }
        
        reject()
      })
      .catch((e) => {
    

        console.log(e)
        reject()
      })
  })
}



const server = {
  loadExercises
}
export default server
